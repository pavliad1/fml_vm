//
// Created by Adam on 19.03.2021.
//

#include "Program.h"
#include "Instruction.h"
#include <map>

void Program::Execute() {

    auto main = ((Method*)(*m_ConstantPool)[m_IP]);
    m_IP = main->Start();
    m_FrameStack.Push(0, 0, main->GetLocalsCount(), m_ArgsStack);

    while (m_IP < (uint16_t)m_Code.size()) {
        auto instruction = m_Code[m_IP];
        //cout << "executing instruction " << instruction->ToString() << endl;
      //  try {
            instruction->Execute(*this);
       // }
       /* catch (const IndexOutOfRange& e) {
            cout << e.what() << endl;
            cout << m_IP << " " << instruction->ToString() << endl;
            throw e;
        }*/
    }

    // cout << "Program has ended successfully!!\n";
}

void Program::IncrementIP() {
    m_IP++;
}

void Program::JumpToIP(Address instruction) {
    m_IP = instruction;
}

void Program::AlterSetAndGetGlobal() {

    typedef map<uint16_t, uint16_t> uint_map;
    auto fn = [](uint_map& m, uint16_t& index, uint16_t gIndex) {
        auto find = m.find(gIndex);
        if (find == m.end()) {
            m.insert({gIndex, index});
            auto ret = index;
            index++;
            return ret;
        }
        return find->second;
    };

    uint_map m;
    uint16_t index = 0;

    for ( auto inst : m_Code ) {
        if (SetGlobal* g = dynamic_cast<SetGlobal*>(inst))
            g->SetIndex(fn(m, index, g->GetIndex()));
        else if (GetGlobal* g = dynamic_cast<GetGlobal*>(inst))
            g->SetIndex(fn(m, index, g->GetIndex()));
    }
}

void Program::AlterCallFunction() {

    //TODO: probably argument count checking

    for ( auto inst : m_Code ) {
        if (CallFunction* f = dynamic_cast<CallFunction*>(inst)) {
            auto cpIndex = f->GetIndex();

            for (int i = 0; i < m_ConstantPool->Size(); i++) {
                if (Method* m = dynamic_cast<Method*>(m_ConstantPool->operator[](i))) {
                    auto mIndex = m->GetIndex();
                    if (mIndex == cpIndex) {
                        f->SetIndex(i);
                        break;
                    }
                }
            }
        }
    }
}

void Program::AlterBranchAndJump() {

    auto findLabel = [&](uint16_t index) {
        for (auto i = 0; i < (int) m_Code.size(); i++) {
            if (m_Code[i]->IsLabel()) {
                auto labelIndex = ((Label *) m_Code[i])->GetIndex();
                if (index == labelIndex)
                    return i;
            }
        }
        throw runtime_error("Instruction points to non-existent Label!");
    };

    //TODO: can add +1 since instruction Label is void and just skips to the next anyway
    for ( auto inst : m_Code ) {
        if (Jump * j = dynamic_cast<Jump*>(inst))
            j->SetIndex(findLabel(j->GetIndex()));
        else if (Branch * b = dynamic_cast<Branch*>(inst))
            b->SetIndex(findLabel(b->GetIndex()));
    }
}

ConstantPool* Program::CreateCP(Reader& reader) {
    int len = (int)reader.ReadWord();
    int ip = 0;
    ConstantPool* cp = new ConstantPool(len);

    //  cout << "There's " << len << " constants.\n";
    for (int i = 0; i < len; i++) {
        auto tag = reader.ReadByte();
        switch (tag) {
            case Integer::TAG: {
                auto value = reader.ReadDWord();
                cp->PushBack(new Integer((int)value));
                break;
            }
            case Null::TAG: {
                cp->PushBack(new Null());
                break;
            }
            case String::TAG: {
                auto len = reader.ReadDWord();
                string value = "";
                for (int i = 0; i < (int)len; i++)
                    value += (char)reader.ReadByte();

                cp->PushBack(new String(value));
                break;
            }
            case Method::TAG: {
                auto name = reader.ReadWord();
                auto argc = reader.ReadByte();
                auto locals = reader.ReadWord();
                auto codeLength = reader.ReadDWord();

                CreateInstructions(reader, codeLength);
                auto start = ip;
                ip += codeLength;

                cp->PushBack(new Method(name, argc, locals, start, ip - 1));
                break;
            }
            case Slot::TAG: {
                auto index = reader.ReadWord();

                cp->PushBack(new Slot(index));
                break;
            }
            case Class::TAG: {
                auto len = reader.ReadWord();
                auto cpRef = (*cp);

                Class::Fields  fields;
                Class::Methods methods;

                size_t fieldCount = 0;
                for (int i = 0; i < (int)len; i++) {
                    auto cpElem = cpRef[reader.ReadWord()];
                    if (cpElem->IsSlot()) {
                        const auto& slotName = cpRef[cpElem->GetSlot()->GetIndex()]->GetString()->GetValue();
                        fields.insert({slotName, fieldCount});
                        fieldCount++;
                    }
                    else if (cpElem->IsMethod()) {
                        auto method = cpElem->GetMethod();
                        const auto& methodName = cpRef[method->GetIndex()]->GetString()->GetValue();
                        methods.insert({methodName, method});
                    }
                    else
                        throw runtime_error("Error serializing Class.");
                }

                cp->PushBack(new Class(fields, methods));
                break;
            }
            case Boolean::TAG: {
                auto val = reader.ReadByte();

                cp->PushBack(new Boolean((bool)val));
                break;
            }
            default:
                throw std::exception();
        }
        //  cout << "Tag: " << hex << setw(2) << (int)tag << endl;
    }
/*
    cout << cp->ToString();

    for (int i = 0; i < ip; i++) {
        cout << to_string(i) << ": " << m_Code[i]->ToString() << endl;
    }*/

    return cp;
}

void Program::CreateInstructions(Reader& reader, uint32_t len) {
    for (int i = 0; i < len; i++) {
        auto tag = reader.ReadByte();
        switch (tag) {
            case Literal::TAG: {
                m_Code.push_back(new Literal(reader.ReadWord()));
                break;
            }
            case Print::TAG: {
                m_Code.push_back(new Print(reader.ReadWord(), reader.ReadByte()));
                break;
            }
            case Drop::TAG: {
                m_Code.push_back(new Drop);
                break;
            }
            case GetLocal::TAG: {
                m_Code.push_back(new GetLocal(reader.ReadWord()));
                break;
            }
            case SetLocal::TAG: {
                m_Code.push_back(new SetLocal(reader.ReadWord()));
                break;
            }
            case GetGlobal::TAG: {
                m_Code.push_back(new GetGlobal(reader.ReadWord()));
                break;
            }
            case SetGlobal::TAG: {
                m_Code.push_back(new SetGlobal(reader.ReadWord()));
                break;
            }
            case Label::TAG: {
                m_Code.push_back(new Label(reader.ReadWord()));
                break;
            }
            case Jump::TAG: {
                m_Code.push_back(new Jump(reader.ReadWord()));
                break;
            }
            case Branch::TAG: {
                m_Code.push_back(new Branch(reader.ReadWord()));
                break;
            }
            case CallMethod::TAG: {
                m_Code.push_back(new CallMethod(reader.ReadWord(), reader.ReadByte()));
                break;
            }
            case CallFunction::TAG: {
                m_Code.push_back(new CallFunction(reader.ReadWord(), reader.ReadByte()));
                break;
            }
            case Return::TAG: {
                m_Code.push_back(new Return());
                break;
            }
            case Array::TAG: {
                m_Code.push_back(new Array());
                break;
            }
            case Object::TAG: {
                m_Code.push_back(new Object(reader.ReadWord()));
                break;
            }
            case GetField::TAG: {
                m_Code.push_back(new GetField(reader.ReadWord()));
                break;
            }
            case SetField::TAG: {
                m_Code.push_back(new SetField(reader.ReadWord()));
                break;
            }
            default:
                throw runtime_error("Instruction " + to_string(tag) + " NYI");
        }
    }
}

void Program::CreateGlobalsAndEntry(Reader &reader) {

    auto globalsLength = reader.ReadWord();
    //cout << "globals: " << globalsLength << endl;
    for (int i = 0; i < globalsLength; i++) {
        auto g = reader.ReadWord();
        //cout << i << ": #" << g << endl;
        m_Globals.push_back(g);
    }

    m_IP = reader.ReadWord();
    //cout << "IP: " <<m_IP <<endl;
}

// i = local 1
// j = local 2
// h = local 0
