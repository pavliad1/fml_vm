//
// Created by Adam on 19.03.2021.
//

#ifndef FML_VC_CONSTANTPOOL_H
#define FML_VC_CONSTANTPOOL_H

#include <vector>
#include <string>
#include "Constant.h"

class ConstantPool {
public:
    ConstantPool(int size);
    ~ConstantPool();

    size_t Size() const;
    void PushBack(Constant* object);
    Constant* operator[](uint16_t index);

    std::string ToString() const;
private:
    std::vector<Constant*> m_Data;
};


#endif //FML_VC_CONSTANTPOOL_H
