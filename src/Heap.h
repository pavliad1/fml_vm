//
// Created by Adam on 19.03.2021.
//

#ifndef FML_VC_HEAP_H
#define FML_VC_HEAP_H

#include "RuntimeObject.h"
#include <stdexcept>
#include <map>
#include <cstdint>

using namespace std;

//TODO: change to shared_ptr

typedef uint32_t Pointer;

struct NotEnoughMemoryException : public exception {
    NotEnoughMemoryException(size_t size, size_t objectSize) : m_Size(size), m_ObjectSize(objectSize) {}
    const char* what() const throw () {
        return "Can't allocate object, memory limit exceeded!";
    }
    size_t m_Size;
    size_t m_ObjectSize;
};

//TODO: FIX THAT STRING + MAYBE OTHER RO TO BE COMPLETELY IN BUFFER

class Heap {
public:
    Heap(size_t size) : m_Size(size), m_LastPointer(0) {
        m_Buffer = new char[size];
    }

    ~Heap() {
        delete[] m_Buffer;
    }

    RuntimeObject *Get(Pointer ptr) const {
        // TODO: add checking
        return (RuntimeObject*)(m_Buffer + ptr);
    }

    template <typename T, typename... Args>
    Pointer AllocateRuntimeObject(Args&&... args) {
        auto size = sizeof(T);
        if (m_LastPointer + size > m_Size)
            throw NotEnoughMemoryException(m_Size, size);

        auto ptr = m_LastPointer;
        new(m_Buffer + m_LastPointer) T(std::forward<Args>(args)...);
        m_LastPointer += size;

        return ptr;
    }

private:
    size_t  m_Size;
    Pointer m_LastPointer;
    char*   m_Buffer;
};


#endif //FML_VC_HEAP_H
