//
// Created by Adam on 19.03.2021.
//

#include "RuntimeObject.h"
#include "Program.h"
#include <sstream>

void RuntimeObject::CallBuiltinMethod(const string &name, Program &program) {
    throw runtime_error(ToString() + " has no method \"" + name + "\".");
}

void RONull::CallBuiltinMethod(const string &name, Program& program) {
    auto& argsStack = program.GetArgumentStack();
    auto& heap      = program.GetHeap();

    if (argsStack.empty())
        throw runtime_error("CallBuiltinMethod of Null requires one argument on operand stack.");

    auto argument = heap.Get(argsStack.back());
    argsStack.pop_back(); // pop arg1
    argsStack.pop_back(); // pop receiver
    if (name == "==" || name == "eq")
        argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>( Equals(argument) ));
    else if (name == "!=" || name == "neq")
        argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>( !Equals(argument) ));
    else
        RuntimeObject::CallBuiltinMethod(name, program);
}

void ROBoolean::CallBuiltinMethod(const string &name, Program &program) {
    auto& argsStack = program.GetArgumentStack();
    auto& heap      = program.GetHeap();

    if (argsStack.empty())
        throw runtime_error("CallBuiltinMethod of Boolean requires one argument on operand stack.");

    auto argument = heap.Get(argsStack.back());
    argsStack.pop_back(); // pop arg1
    argsStack.pop_back(); // pop receiver
    if (name == "==" || name == "eq")
        argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>( Equals(argument) ));
    else if (name == "!=" || name == "neq")
        argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>( !Equals(argument) ));
    else if (name == "&" || name == "and")
        argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>( And(argument) ));
    else if (name == "|" || name == "or")
        argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>( Or(argument) ));
    else
        RuntimeObject::CallBuiltinMethod(name, program);
}

void ROInteger::CallBuiltinMethod(const string &name, Program &program) {
    auto& argsStack = program.GetArgumentStack();
    auto& heap      = program.GetHeap();

    if (argsStack.empty())
        throw runtime_error("CallBuiltinMethod of Integer requires one argument on operand stack.");

    auto argument = heap.Get(argsStack.back());
    argsStack.pop_back(); // pop arg1
    argsStack.pop_back(); // pop receiver
    if (name == "==" || name == "eq")
        argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>( Equals(argument) ));
    else if (name == "!=" || name == "neq")
        argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>( !Equals(argument) ));
    else {
        if (!argument->IsInteger())
            throw runtime_error("Method " + name + " of Integer requires Integer argument on operand stack.");

        ROInteger* arg = argument->GetInteger();

        if (name == "<" || name == "lt")
            argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>(m_Value < arg->m_Value));
        else if (name == ">" || name == "gt")
            argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>(m_Value > arg->m_Value));
        else if (name == "<=" || name == "le")
            argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>(m_Value <= arg->m_Value));
        else if (name == ">=" || name == "ge")
            argsStack.push_back(heap.AllocateRuntimeObject<ROBoolean>(m_Value >= arg->m_Value));
        else if (name == "+" || name == "add")
            argsStack.push_back(heap.AllocateRuntimeObject<ROInteger>(m_Value + arg->m_Value));
        else if (name == "-" || name == "sub")
            argsStack.push_back(heap.AllocateRuntimeObject<ROInteger>(m_Value - arg->m_Value));
        else if (name == "*" || name == "mul")
            argsStack.push_back(heap.AllocateRuntimeObject<ROInteger>(m_Value * arg->m_Value));
        else if (name == "/" || name == "div")
            argsStack.push_back(heap.AllocateRuntimeObject<ROInteger>(m_Value / arg->m_Value));
        else if (name == "%" || name == "mod")
            argsStack.push_back(heap.AllocateRuntimeObject<ROInteger>(m_Value % arg->m_Value));
        else
            RuntimeObject::CallBuiltinMethod(name, program);
    }
}

void ROArray::CallBuiltinMethod(const string &name, Program &program) {
    auto& argsStack = program.GetArgumentStack();
    auto& heap      = program.GetHeap();

    if (name == "get") {
        if (argsStack.empty())
            throw runtime_error("CallBuiltinMethod \"get\" of Array requires 1 argument on the stack.");

        auto index = heap.Get(argsStack.back());
        if (!index->IsInteger() || index->GetInteger()->Value() < 0)
            throw runtime_error("CallBuiltinMethod \"get\" of Array requires positive Integer argument on stack.");

        argsStack.pop_back(); // pop argument
        argsStack.pop_back(); // pop receiver
        argsStack.push_back(Get(index->GetInteger()->Value()));
    }
    else if (name == "set") {
        if (argsStack.size() < 2)
            throw runtime_error("CallBuiltinMethod \"set\" of Array requires 2 arguments on the stack.");

        auto value = argsStack.back();
        argsStack.pop_back(); // pop arg1(value)

        auto index = heap.Get(argsStack.back());
        if (!index->IsInteger() || index->GetInteger()->Value() < 0)
            throw runtime_error("CallBuiltinMethod \"set\" of Array requires positive Integer argument on stack.");

        argsStack.pop_back(); // pop arg2(index)
        argsStack.pop_back(); // pop receiver

        argsStack.push_back(Set(index->GetInteger()->Value(), value));
    }
    else
        RuntimeObject::CallBuiltinMethod(name, program);
}

void ROObject::CallBuiltinMethod(const string &name, Program &program) {
    // Object's does not have built-in methods, so it's called recursively until it reached Null/Integer/Boolean/Array
    program.GetHeap().Get(m_Parent)->CallBuiltinMethod(name, program);
}

bool ROBoolean::And(RuntimeObject *other) const {
    if (!other->IsBoolean())
        throw runtime_error("CallBuiltinMethod & requires it's argument to be Boolean.");

    return m_Value && other->GetBoolean()->m_Value;
}

bool ROBoolean::Or(RuntimeObject *other) const {
    if (!other->IsBoolean())
        throw runtime_error("CallBuiltinMethod | requires it's argument to be Boolean.");

    return m_Value || other->GetBoolean()->m_Value;
}

string ROArray::ToString(Program *program) const {

    if (!program)
        return "Array";

    stringstream s;
    s << "[";
    for (int i = 0; i < m_Size; i++) {
        s << program->GetHeap().Get(m_Values[i])->ToString(program);
        if (i < m_Size - 1)
            s << ", ";
    }
    s << "]";
    return s.str();
}

std::pair<const ROObject*, size_t> ROObject::GetFieldIndex(const string &fieldName, Program &program) const {
    auto& heap = program.GetHeap();
    auto& cp   = program.GetCP();

    auto& fields = cp[m_Index]->GetClass()->GetFields();
    auto field = fields.find(fieldName);

    // we could not find field "fieldName" in this object
    if (field == fields.end()) {
        auto parent = heap.Get(m_Parent);

        // delegate to parent if parent is Object
        if (parent->IsObject())
            return parent->GetObject()->GetFieldIndex(fieldName, program);

        // otherwise throw an error since only objects can have fields
        throw runtime_error("This object has no field \"" + fieldName + "\".");
    }

    // should check if index is in range here?
    return {this, field->second};
}


Pointer ROObject::GetField(const string &fieldName, Program &program) const {
    auto& heap = program.GetHeap();
    auto& cp   = program.GetCP();

    auto objectAndOffset = GetFieldIndex(fieldName, program);
    return objectAndOffset.first->m_Fields[objectAndOffset.second];
}

Pointer ROObject::SetField(const string &fieldName, Pointer value, Program &program) {
    auto& heap = program.GetHeap();
    auto& cp   = program.GetCP();

    auto objectAndOffset = GetFieldIndex(fieldName, program);
    objectAndOffset.first->m_Fields[objectAndOffset.second] = value;
    return objectAndOffset.first->m_Fields[objectAndOffset.second];
}


std::pair<Pointer,Method*> ROObject::GetReceiver(Pointer curPointer, const string &methodName, Program &program) const {
    auto& heap = program.GetHeap();
    auto& cp   = program.GetCP();

    auto methods = cp[m_Index]->GetClass()->GetMethods();

    // if this one does not have method "methodName", continue up in hierarchy
    auto ret = methods.find(methodName);

    if (ret == methods.end())
        return heap.Get(m_Parent)->GetReceiver(m_Parent, methodName, program);

    // we found object with our method, return Pointer to it !
    return {curPointer, ret->second};
}

string ROObject::ToString(Program *program) const {

    if (!program)
        return "Object";

    auto& fields = program->GetCP()[m_Index]->GetClass()->GetFields();
    auto parent = program->GetHeap().Get(m_Parent)->GetObject();
    stringstream s;
    s << "object(";
    if (parent) {
        s << "..=" << parent->ToString(program);
        if (!fields.empty())
            s << ", ";
    }
    // so its not sorted alphabetically but rather by order in bytecode
    std::vector<std::pair<const string*, size_t>> vec(fields.size());
    for (const auto& f : fields) {
        vec[f.second] = {&f.first, f.second};
    }
    for (size_t i = 0; i < vec.size(); i++) {
        s << (*vec[i].first) << "=" << program->GetHeap().Get(m_Fields[vec[i].second])->ToString(program);
        if (i < fields.size() - 1)
            s << ", ";
    }
    /*int i = 0;
    for (const auto& f : fields) {
        s << f.first << "(" << f.second << ")" << "=" << program->GetHeap().Get(m_Fields[f.second])->ToString(program);
        if (i < fields.size() - 1)
            s << ", ";
        i++;
    }*/
    s << ")";
    return s.str();
}
