//
// Created by Adam on 17.03.2021.
//

#ifndef FML_VC_CONSTANT_H
#define FML_VC_CONSTANT_H

#include <string>
#include <vector>
#include <set>
#include "RuntimeObject.h"
#include "Heap.h"

using namespace std;

class Integer;
class Boolean;
class Null;
class String;
class Class;
class Method;
class Slot;

class Constant {
public:

    Constant() = default;
    virtual std::string ToString() const = 0;
    virtual Pointer InstantiateRuntimeObject(Heap& heap) const {
        throw runtime_error("Abstract class \"Constant\" can not be instantiated!");
    }

    virtual bool IsInteger() const { return false; }
    virtual bool IsBoolean() const { return false; }
    virtual bool IsNull() const { return false; }
    virtual bool IsString() const { return false; }
    virtual bool IsClass() const { return false; }
    virtual bool IsMethod() const { return false; }
    virtual bool IsSlot() const { return false; }

    virtual Integer* GetInteger() { return nullptr; }
    virtual Boolean* GetBoolean() { return nullptr; }
    virtual Null* GetNull() { return nullptr; }
    virtual String* GetString() { return nullptr; }
    virtual Class* GetClass() { return nullptr; }
    virtual Method* GetMethod() { return nullptr; }
    virtual Slot* GetSlot() { return nullptr; }
private:
};

class Integer : public Constant {
public:
    static const uint8_t TAG = 0x00;

    Integer(int value) : m_Value(value) {}
    std::string ToString() const { return std::to_string(m_Value); }

    Pointer InstantiateRuntimeObject(Heap& heap) const override {
        return heap.AllocateRuntimeObject<ROInteger>(m_Value);
    }

    bool IsInteger() const override {
        return true;
    }

    Integer *GetInteger() override {
        return this;
    }


private:
    int m_Value;
};

class Boolean : public Constant {
public:
    static const uint8_t TAG = 0x06;
    Boolean(bool value) : m_Value(value) {}
    std::string ToString() const { return m_Value ? "true" : "false"; }

    Pointer InstantiateRuntimeObject(Heap& heap) const override {
        return heap.AllocateRuntimeObject<ROBoolean>(m_Value);
    }

    bool IsBoolean() const override {
        return true;
    }

    Boolean *GetBoolean() override {
        return this;
    }

private:
    bool m_Value;
};

class Null : public Constant {
public:
    static const uint8_t TAG = 0x01;
    Null() = default;
    std::string ToString() const { return "null"; }

    Pointer InstantiateRuntimeObject(Heap& heap) const override {
        return heap.AllocateRuntimeObject<RONull>();
    }

    bool IsNull() const override {
        return true;
    }

    Null *GetNull() override {
        return this;
    }

private:
};

class String : public Constant {
public:
    static const uint8_t TAG = 0x02;
    String(const std::string &value) : m_Value(value) {
        vector<pair<string, string>> replacements = {{"\\n",  "\n"},
                                                     {"\\r",  "\r"},
                                                     {"\\t",  "\t"},
                                                     {"\\\\", "\\"},
                                                     {"\\~",  "~"},
                                                     {"\\\"", "\""}};
        m_Formatted = value;
        for ( const auto& fromTo : replacements )
            m_Formatted = replaceAll(m_Formatted, fromTo.first, fromTo.second);

        // TODO: error when encountering other escape sequence than listed here

    };
    std::string ToString() const { return "\"" + m_Value + "\""; }

    const string& GetFormat() const { return m_Formatted; }

    const string& GetValue() const { return m_Value; }

    bool IsString() const override {
        return true;
    }

    String *GetString() override {
        return this;
    }

    Pointer InstantiateRuntimeObject(Heap& heap) const override {
        return heap.AllocateRuntimeObject<ROString>(m_Value);
    }
private:

    // https://gist.github.com/GenesisFR/cceaf433d5b42dcdddecdddee0657292
    string replaceAll(string str, const string &from, const string &to) {
        size_t start_pos = 0;
        while ((start_pos = str.find(from, start_pos)) != string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
        }
        return str;
    }

    std::string m_Value;
    std::string m_Formatted;
};

class Slot : public Constant {
public:
    static const uint8_t TAG = 0x04;
    Slot(uint16_t index) : m_Index(index) {}
    std::string ToString() const { return "slot #" + to_string(m_Index); }

    uint16_t GetIndex() const {
        return m_Index;
    }

    bool IsSlot() const override {
        return true;
    }

    Slot *GetSlot() override {
        return this;
    }

private:
    uint16_t m_Index;
};

class Method : public Constant {
public:
    static const uint8_t TAG = 0x03;
    Method(uint16_t index, uint8_t argc, uint16_t localsCount, uint32_t start, uint32_t end) : m_Index(index), m_Argc(argc), m_LocalsCount(localsCount), m_Start(start), m_End(end) {}
    std::string ToString() const {
        return "method: #" + std::to_string(m_Index) +
               " argc:" + std::to_string(m_Argc) +
               " locals:" + std::to_string(m_LocalsCount) +
               " " + std::to_string(m_Start) + "-" + std::to_string(m_End);
    }
    uint16_t Start() const { return m_Start; }

    bool IsMethod() const override {
        return true;
    }

    Method *GetMethod() override {
        return this;
    }

    uint16_t GetIndex() const {
        return m_Index;
    }

    uint8_t GetArgc() const {
        return m_Argc;
    }

    uint16_t GetLocalsCount() const {
        return m_LocalsCount;
    }

    uint32_t GetStart() const {
        return m_Start;
    }

    uint32_t GetEnd() const {
        return m_End;
    }

private:
    uint16_t    m_Index;
    uint8_t     m_Argc;
    uint16_t    m_LocalsCount;
    uint32_t    m_Start;
    uint32_t    m_End;
};

class Class : public Constant {
public:
    static const uint8_t TAG = 0x05;

    using Fields  = std::map<string, size_t>;  // this maps fields name to index inside object's instance
    using Methods = std::map<string, Method*>; // this maps method names to its constant pool object

    Class (const Fields& fields, const Methods& methods) : m_Fields(fields), m_Methods(methods) {}

    std::string ToString() const {
        string str = "class (fields: ";
        for (const auto& f: m_Fields)
            str += f.first + " ";
        str += ", methods: ";
        for (const auto& f: m_Methods)
            str += f.first + " ";
        return str + ")";
    }

    bool IsClass() const override {
        return true;
    }

    Class *GetClass() override {
        return this;
    }

    const Fields& GetFields() const {
        return m_Fields;
    }

    const Methods& GetMethods() const {
        return m_Methods;
    }

private:
    Fields  m_Fields;
    Methods m_Methods;
};


#endif //FML_VC_CONSTANT_H
