//
// Created by Adam on 19.03.2021.
//

#ifndef FML_VC_INSTRUCTION_H
#define FML_VC_INSTRUCTION_H

#include <string>
#include <stack>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include "ConstantPool.h"
#include "Heap.h"
#include "Program.h"

using namespace std;

class Instruction {
public:
    virtual std::string ToString() const = 0;

    virtual void Execute(Program &program) = 0;

    virtual bool IsLabel() const { return false; }

    virtual bool IsJump() const { return false; }

private:
};

class Literal : public Instruction {
public:
    static const uint8_t TAG = 0x01;

    Literal(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "lit #" + std::to_string(m_Index); }

    void Execute(Program &program) override {

        auto &argsStack = program.GetArgumentStack();
        auto &heap = program.GetHeap();
        auto &cp = program.GetCP();

        argsStack.push_back(cp[m_Index]->InstantiateRuntimeObject(heap));
        program.IncrementIP();

        //cout << argsStack.top() << " " << heap.Get(argsStack.top())->ToString() << endl;
    }

private:
    uint16_t m_Index;
};

class Print : public Instruction {
public:
    static const uint8_t TAG = 0x02;

    Print(uint16_t index, uint8_t argc) : m_Index(index), m_Argc(argc) {}

    std::string ToString() const override {
        return "printf #" + std::to_string(m_Index) + " " + std::to_string(m_Argc);
    }

    void Execute(Program &program) override {
        //TODO: check if theres actaully string at m_Index

        auto &cp = program.GetCP();
        auto &argsStack = program.GetArgumentStack();
        auto &heap = program.GetHeap();

        const auto &format = cp[m_Index]->GetString()->GetFormat();

        if (argsStack.size() < m_Argc)
            throw runtime_error("Not enough arguments to printf on stack.");

        stack<RuntimeObject *> args;
        for (int i = 0; i < m_Argc; i++) {
            args.push(heap.Get(argsStack.back()));
            argsStack.pop_back();
        }

        stringstream stream;
        int pos = 0;
        for (const auto &c : format) {
            if (c == '~') {
                if (pos < m_Argc) {
                    stream << args.top()->ToString(&program);
                    pos++;
                    args.pop();
                }
            } else
                stream << c;
        }

        cout << stream.str();
        argsStack.push_back(heap.AllocateRuntimeObject<RONull>());
        program.IncrementIP();
    }

private:
    uint16_t m_Index;
    uint8_t m_Argc;
};

class Drop : public Instruction {
public:
    static const uint8_t TAG = 0x10;

    Drop() = default;

    std::string ToString() const override { return "drop"; }

    void Execute(Program &program) override {

        auto &argsStack = program.GetArgumentStack();

        if (argsStack.empty())
            throw std::runtime_error("drop requires at least one element on stack.");

        argsStack.pop_back();
        program.IncrementIP();
    }

private:
};

class Label : public Instruction {
public:
    static const uint8_t TAG = 0x00;

    Label(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "label #" + to_string(m_Index); }

    // methods used to alter this instruction
    uint16_t GetIndex() const { return m_Index; }

    const string &GetLabel(ConstantPool &cp) const {
        return cp[m_Index]->GetString()->GetValue();
    }

    bool IsLabel() const override {
        return true;
    }

    void Execute(Program &program) override {
        program.IncrementIP();
    }

private:
    uint16_t m_Index;
};

class Jump : public Instruction {
public:
    static const uint8_t TAG = 0x0E;

    Jump(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "jump #" + to_string(m_Index); }

    bool IsJump() const override {
        return true;
    }

    // methods used to alter this instruction
    void SetIndex(uint16_t newIndex) { m_Index = newIndex; }

    uint16_t GetIndex() const { return m_Index; }

    void Execute(Program &program) override {
        program.JumpToIP(m_Index);
    }

private:
    uint16_t m_Index;
};

class Branch : public Instruction {
public:
    static const uint8_t TAG = 0x0D;

    Branch(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "branch #" + to_string(m_Index); }

    // methods used to alter this instruction
    void SetIndex(uint16_t newIndex) { m_Index = newIndex; }

    uint16_t GetIndex() const { return m_Index; }

    void Execute(Program &program) override {

        auto &argsStack = program.GetArgumentStack();
        auto &heap = program.GetHeap();

        if (argsStack.empty())
            throw runtime_error("Branch requires argument on stack.");

        const auto &cond = heap.Get(argsStack.back());
        argsStack.pop_back();
        if (cond->IsNull() || (cond->IsBoolean() && !cond->GetBoolean()->Value()))
            program.IncrementIP();
        else
            program.JumpToIP(m_Index);
    }

private:
    uint16_t m_Index;
};

class SetLocal : public Instruction {
public:
    static const uint8_t TAG = 0x09;

    SetLocal(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "set local #" + to_string(m_Index); }

    void Execute(Program &program) override {

        auto &argsStack = program.GetArgumentStack();
        auto &frameStack = program.GetFrameStack();

        if (argsStack.empty())
            throw runtime_error("Set Local requires argument on stack.");

        frameStack.SetLocal(m_Index, argsStack.back());
        program.IncrementIP();
    }

private:
    uint16_t m_Index;
};

class GetLocal : public Instruction {
public:
    static const uint8_t TAG = 0x0A;

    GetLocal(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "get local #" + to_string(m_Index); }

    void Execute(Program &program) override {

        auto &argsStack = program.GetArgumentStack();
        auto &frameStack = program.GetFrameStack();

        argsStack.push_back(frameStack.GetLocal(m_Index));

        //cout << "GetLocal stack top: " << program.GetHeap().Get(argsStack.back())->ToString() << endl;

        program.IncrementIP();

    }

private:
    uint16_t m_Index;
};

class SetGlobal : public Instruction {
public:
    static const uint8_t TAG = 0x0B;

    SetGlobal(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "set global #" + to_string(m_Index); }

    // methods used to alter this instruction
    void SetIndex(uint16_t newIndex) { m_Index = newIndex; }

    uint16_t GetIndex() const { return m_Index; }

    void Execute(Program &program) override {

        auto &argsStack = program.GetArgumentStack();
        auto &globals = program.GetGlobals();
        auto &frameStack = program.GetFrameStack();
        auto &heap = program.GetHeap();

        if (argsStack.empty())
            throw runtime_error("Set Global requires argument on stack.");

        // TODO: check globals.size

        frameStack.SetGlobal(m_Index, argsStack.back());
        program.IncrementIP();

        //cout << " GLOBAL[" << m_Index << "] = " << frameStack.GetGlobal(m_Index) << endl;//heap.Get(frameStack.GetGlobal(m_Index))->ToString() << endl;

    }

private:
    uint16_t m_Index;
};

class GetGlobal : public Instruction {
public:
    static const uint8_t TAG = 0x0C;

    GetGlobal(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "get global #" + to_string(m_Index); }

    // methods used to alter this instruction
    void SetIndex(uint16_t newIndex) { m_Index = newIndex; }

    uint16_t GetIndex() const { return m_Index; }

    void Execute(Program &program) override {

        auto &frameStack = program.GetFrameStack();
        auto &argsStack = program.GetArgumentStack();
        auto &globals = program.GetGlobals();

        //cout << "get global " << m_Index << " -> " << frameStack.GetGlobal(m_Index) << endl;

        argsStack.push_back(frameStack.GetGlobal(m_Index));
        program.IncrementIP();

    }

private:
    uint16_t m_Index;
};

// this map contains built-in method names and number of their arguments
const map<string, uint8_t> builtinMethods = {
        {"==", 1},
        {"eq", 1},
        {"!=", 1},
        {"neq", 1},
        {"+", 1},
        {"add", 1},
        {"-", 1},
        {"sub", 1},
        {"*", 1},
        {"mul", 1},
        {"/", 1},
        {"div", 1},
        {"%", 1},
        {"mod", 1},
        {"<=", 1},
        {"le", 1},
        {">=", 1},
        {"ge", 1},
        {"<", 1},
        {"lt", 1},
        {">", 1},
        {"gt", 1},
        {"==", 1},
        {"&", 1},
        {"and", 1},
        {"|", 1},
        {"or", 1},
        {"get", 1},
        {"set", 2}
};

class CallMethod : public Instruction {
public:
    static const uint8_t TAG = 0x07;

    CallMethod(uint16_t index, uint8_t argc) : m_Index(index), m_Argc(argc) {}

    std::string ToString() const override { return "call method #" + to_string(m_Index) + " " + to_string(m_Argc); }

    void SetIndex(uint16_t newIndex) { m_Index = newIndex; }

    uint16_t GetIndex() const { return m_Index; }

    void Execute(Program &program) override {
        auto &frameStack = program.GetFrameStack();
        auto &argsStack = program.GetArgumentStack();
        auto &heap = program.GetHeap();
        auto &cp = program.GetCP();

        const auto &methodName = cp[m_Index]->GetString()->GetValue();

        auto method = FindMethod(m_Index, cp);
        auto builtin = builtinMethods.find(methodName);
        // we found actual method
        if (method) {
            //cout << "we found method!\n";

            auto argsCount = method->GetArgc();
            if (argsStack.size() < argsCount)
                throw runtime_error("CallMethod2 not enough arguments on operand stack.");

            auto receiverPtr = argsStack[argsStack.size() - argsCount];
            auto receiver = heap.Get(receiverPtr);

            if (receiver->IsObject()) {
                // we need to get actual receiver of the method, which is an object in the hierarchy
                // that has "methodName" defined in its methods
                auto actualReceiverAndMethod = receiver->GetReceiver(receiverPtr, methodName, program);


                // if the actualReceiver is not the same as receiver on the stack, replace it
                if (actualReceiverAndMethod.first != receiverPtr)
                    argsStack[argsStack.size() - argsCount] = actualReceiverAndMethod.first;

                method = actualReceiverAndMethod.second;

                frameStack.Push(program.GetIP() + 1, method->GetArgc(), method->GetLocalsCount(),
                                program.GetArgumentStack());
                program.JumpToIP(method->Start());
                return;
            }
        }

        if (builtin == builtinMethods.end())
            throw runtime_error("CallMethod can't find method \"" + methodName + "\".");

        size_t argsCount = builtin->second;
        if (argsStack.size() < argsCount)
            throw runtime_error("CallMethod1 not enough arguments on operand stack.");

        auto receiver = heap.Get(argsStack[argsStack.size() - argsCount - 1]);
        receiver->CallBuiltinMethod(methodName, program);
        program.IncrementIP();
    }

private:

    Method* FindMethod(uint16_t nameIndex, ConstantPool& cp) const {
        for (size_t i = 0; i < cp.Size(); i++) {
            auto constant = cp[i];
            if (constant->IsMethod() && constant->GetMethod()->GetIndex() == nameIndex)
                return constant->GetMethod();
        }
        return nullptr;
    }

    uint16_t m_Index;
    uint8_t m_Argc;
};

class CallFunction : public Instruction {
public:
    static const uint8_t TAG = 0x08;

    CallFunction(uint16_t index, uint8_t argc) : m_Index(index), m_Argc(argc) {}

    std::string ToString() const override { return "call function #" + to_string(m_Index) + " " + to_string(m_Argc); }

    // methods used to alter this instruction
    void SetIndex(uint16_t newIndex) { m_Index = newIndex; }

    uint16_t GetIndex() const { return m_Index; }

    void Execute(Program &program) override {
        auto &frameStack = program.GetFrameStack();
        auto &cp = program.GetCP();

        auto method = cp[m_Index]->GetMethod();
        frameStack.Push(program.GetIP() + 1, method->GetArgc(), method->GetLocalsCount(), program.GetArgumentStack());

        //frameStack.PrintCurrentFrame();

        program.JumpToIP(method->Start());
    }

private:
    uint16_t m_Index; // after calling Alter in program it points right to Constant that is Method
    uint8_t m_Argc;
};

class Return : public Instruction {
public:
    static const uint8_t TAG = 0x0F;

    Return() = default;

    std::string ToString() const override { return "return"; }

    void Execute(Program &program) override {
        auto &frameStack = program.GetFrameStack();

        program.JumpToIP(frameStack.Pop());
    }

private:
};

class Array : public Instruction {
public:
    static const uint8_t TAG = 0x03;

    Array() = default;

    std::string ToString() const override { return "array"; }

    void Execute(Program &program) override {
        auto &argsStack = program.GetArgumentStack();
        auto &heap = program.GetHeap();

        if (argsStack.size() < 2)
            throw runtime_error("Instruction Array requires 2 operands on stack.");

        auto initValue = argsStack.back();
        argsStack.pop_back();
        auto size = heap.Get(argsStack.back());
        argsStack.pop_back();

        if (!size->IsInteger() || size->GetInteger()->Value() < 1)
            throw runtime_error("Array requires positive integer argument as its size.");

        argsStack.push_back(heap.AllocateRuntimeObject<ROArray>(size->GetInteger()->Value(), initValue));

        program.IncrementIP();
    }

private:
};

class Object : public Instruction {
public:
    static const uint8_t TAG = 0x04;

    Object(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "object #" + to_string(m_Index); }

    void Execute(Program &program) override {

        auto& argsStack = program.GetArgumentStack();
        auto& heap      = program.GetHeap();
        auto& cp        = program.GetCP();

        auto cpClass = cp[m_Index]->GetClass();
        auto fieldCount = cpClass->GetFields().size();
        if (argsStack.size() < fieldCount + 1)
            throw runtime_error("Object not enough elements on stack.");

        auto parent     = argsStack[argsStack.size() - fieldCount - 1]; // get parent object from stack
        auto object     = heap.AllocateRuntimeObject<ROObject>(m_Index, fieldCount, parent);
        auto heapObject = heap.Get(object)->GetObject();
        for (int i = 0; i < fieldCount; i++) {
            heapObject->SetFieldIndex(fieldCount - i - 1, argsStack.back());
            argsStack.pop_back(); // pop field from stack
        }
        argsStack.pop_back(); // pop parent object from stack
        argsStack.push_back(object);

        program.IncrementIP();
    }

private:
    uint16_t m_Index;
};

class GetField : public Instruction {
public:
    static const uint8_t TAG = 0x05;

    GetField(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "get field #" + to_string(m_Index); }

    void Execute(Program &program) override {

        auto& argsStack  = program.GetArgumentStack();
        auto& cp         = program.GetCP();
        auto& heap       = program.GetHeap();

        if (argsStack.empty())
            throw runtime_error("GetField requires argument on stack.");

        auto object = heap.Get(argsStack.back());
        argsStack.pop_back();
        if (!object->IsObject())
            throw runtime_error("GetField requires Object on top of the stack.");

        auto fieldName = cp[m_Index]->GetString()->GetValue();
        argsStack.push_back(object->GetObject()->GetField(fieldName, program));

        program.IncrementIP();
    }

private:
    uint16_t m_Index;
};

class SetField : public Instruction {
public:
    static const uint8_t TAG = 0x06;

    SetField(uint16_t index) : m_Index(index) {}

    std::string ToString() const override { return "set field #" + to_string(m_Index); }

    void Execute(Program &program) override {

        auto& argsStack  = program.GetArgumentStack();
        auto& cp         = program.GetCP();
        auto& heap       = program.GetHeap();

        if (argsStack.empty())
            throw runtime_error("SetField requires 2 arguments on stack.");
        auto value  = argsStack.back();
        argsStack.pop_back();
        auto object = heap.Get(argsStack.back());
        argsStack.pop_back();
        if (!object->IsObject())
            throw runtime_error("SetField requires Object on top of the stack.");

        auto fieldName = cp[m_Index]->GetString()->GetValue();
        argsStack.push_back(object->GetObject()->SetField(fieldName, value, program));

        program.IncrementIP();
    }

private:
    uint16_t m_Index;
};

#endif //FML_VC_INSTRUCTION_H
