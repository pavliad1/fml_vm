#include <iostream>
#include "Program.h"

using namespace std;

void Help() {
    std::cout << "Usage: program.exe execute input\n";
}

int main(int argc, char* argv[]) {

    if (argc != 3 || std::string(argv[1]) != "execute") {
        Help();
        return 1;
    }

    ifstream in(argv[2], ios::binary);
    if (!in.is_open()) {
        cout << "Can't open source file \"" << argv[1] << "\".";
        return 1;
    }

    Reader reader(in);
    Program p(reader);

    try {
        p.Execute();
    }
    catch (const std::exception& e) {
        cout << e.what() << endl;
    }

    return 0;
}
