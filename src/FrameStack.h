//
// Created by Adam on 24.03.2021.
//

#ifndef FML_VC_FRAMESTACK_H
#define FML_VC_FRAMESTACK_H

#include <stdexcept>
#include <stack>
#include <vector>
#include <memory.h>
#include "Heap.h"
#include <iostream>

using namespace std;

typedef uint16_t Address;

struct FrameStackOverflow : public exception {
    FrameStackOverflow(size_t size) : m_Size(size) {}

    const char *what() const throw() {
        return "Maximum frame stack size exceeded.";
    }

    size_t m_Size;
};

struct IndexOutOfRange : public exception {
    IndexOutOfRange(uint16_t index, uint16_t size) : m_Index(index), m_Size(size) {}
    const char *what() const throw() {
        std::string msg = "Locals: index for current frame exceededs it's size [" + to_string(m_Index) + ", " + to_string(m_Size) + "]";
        return msg.c_str();
    }
    uint16_t m_Index;
    uint16_t m_Size;
};

struct Frame {
    Address ret;
    Address prevBP;
};

class FrameStack {
public:
    FrameStack(size_t size) : m_Size(size), m_SP(0), m_BP(0), m_Buffer(new char[size]) {}

    ~FrameStack() {
        delete[] m_Buffer;
    }

    void CreateGlobalFrame(size_t numGlobals) {
        new(m_Buffer) Frame{0, 0};
        m_GlobalFrameEnd = sizeof(Frame) + numGlobals * sizeof(Pointer);
        m_SP = m_GlobalFrameEnd;
    }

    void Push(Address ret, size_t numArgs, size_t numLocals, vector<Pointer> &args) {
        //TODO: throw FrameStackOverflow if size exceeded

        //cout << "BP " << m_BP <<endl;

        new(m_Buffer + m_SP) Frame{ret, m_BP};
        m_BP = m_SP;
        m_SP += sizeof(Frame);

        // push arguments
        for (int i = 0; i < numArgs; i++) {
            auto addr = m_SP + (numArgs - i - 1) * sizeof(Pointer);
            new(m_Buffer + addr) Pointer(args.back());
            args.pop_back();
        }
        //TODO: locals
        m_SP += (numArgs + numLocals) * sizeof(Pointer);
    }

    Address Pop() {
        auto curFrame = (Frame *) (m_Buffer + m_BP);
        m_SP = m_BP;
        m_BP = curFrame->prevBP;
        return curFrame->ret;
    }

    Pointer GetLocal(uint16_t index) {
        return *(Pointer *) (m_Buffer + GetVariableOffset(m_BP, m_SP, index));
    }

    void SetLocal(uint16_t index, Pointer value) {
        SetVariable(m_BP, m_SP, index, value);
    }

    Pointer GetGlobal(uint16_t index) {
        return *((Pointer*)(m_Buffer + GetVariableOffset(0, m_GlobalFrameEnd, index)));
    }

    void SetGlobal(uint16_t index, Pointer value) {
        SetVariable(0, m_GlobalFrameEnd, index, value);
    }

    void PrintCurrentFrame() const {
        auto frame = (Frame *) (m_Buffer + m_BP);
        cout << "frame: prevBP " << frame->prevBP << " - ret " << frame->ret << endl;
        for (char *addr = (m_Buffer + m_BP + sizeof(Frame)); addr < m_Buffer + m_SP; addr += sizeof(Pointer)) {
            cout << "addr: " << (void *) addr << " = " << *((Pointer *) addr) << endl;
        }
    }

private:
    Pointer GetVariableOffset(Address frameStart, Address frameEnd, uint16_t index) const {

        auto offset = frameStart + sizeof(Frame) + index * sizeof(Pointer);

        if (offset >= frameEnd)
            throw IndexOutOfRange(index, (frameEnd - (frameStart +  sizeof(Frame))) / sizeof(Pointer));

        return offset;
    }

    void SetVariable(Address frameStart, Address frameEnd, uint16_t index, Pointer value) {

        auto offset = GetVariableOffset(frameStart, frameEnd, index);

        memcpy(m_Buffer + offset, &value, sizeof(value));
    }

    size_t m_Size;
    Address m_SP;
    Address m_BP;
    Address m_GlobalFrameEnd;
    char *m_Buffer;
};


#endif //FML_VC_FRAMESTACK_H
