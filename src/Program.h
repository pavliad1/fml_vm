//
// Created by Adam on 19.03.2021.
//

#ifndef FML_VC_PROGRAM_H
#define FML_VC_PROGRAM_H

#include "ConstantPool.h"
#include "Heap.h"
#include <stack>
#include <vector>
#include "FrameStack.h"
#include "Reader.h"

using namespace std;

const size_t HEAP_SIZE = 10000000000;
const size_t FS_SIZE = 1000000;

class Instruction;

class Program {
public:
    Program(Reader& reader) : m_FrameStack(FS_SIZE), m_Heap(HEAP_SIZE) {

        m_ConstantPool = CreateCP(reader);
        CreateGlobalsAndEntry(reader);

        m_FrameStack.CreateGlobalFrame(m_Globals.size());

        AlterSetAndGetGlobal();

        AlterCallFunction();

        AlterBranchAndJump();
    }

    void Execute();

    void IncrementIP();

    void JumpToIP(Address instruction);

    Address GetIP() const { return m_IP; }

    ConstantPool &GetCP() { return *m_ConstantPool; };

    Heap &GetHeap() { return m_Heap; }

    vector<Pointer> &GetArgumentStack() { return m_ArgsStack; }

    FrameStack &GetFrameStack() { return m_FrameStack; }

    vector<uint16_t>& GetGlobals() { return m_Globals; }

    vector<Instruction *>& GetCode() { return m_Code; }

private:

    // methods for serialization from source file
    ConstantPool* CreateCP(Reader& reader);
    void CreateGlobalsAndEntry(Reader& reader);
    void CreateInstructions(Reader& reader, uint32_t codeLength);

    // this method alters all Set/GetGlobal instructions so the m_Index doesnt point to String in Constant Pool but
    // rather it points right away to index in global frame
    void AlterSetAndGetGlobal();

    // alters all CallFunction instructions so that m_Index points directly Method in Constant Pool
    void AlterCallFunction();

    // alters Branch and JumpToIP instructions so that m_Index points to instruction number instead of String in Constant Pool
    void AlterBranchAndJump();

    ConstantPool *m_ConstantPool;
    Heap m_Heap;
    vector<uint16_t> m_Globals;
    vector<Pointer> m_ArgsStack;
    FrameStack m_FrameStack;
    vector<Instruction *> m_Code;
    Address m_IP;
};


#endif //FML_VC_PROGRAM_H
