//
// Created by Adam on 18.03.2021.
//

#ifndef FML_VC_READER_H
#define FML_VC_READER_H

#include <string>
#include <iostream>
#include <fstream>

using namespace std;


// it read little-endian
class Reader {
public:
    Reader(ifstream& input) : m_Input(input) {

    }

    uint8_t ReadByte() {
        uint8_t c = m_Input.get();
        return c;
    }

    uint16_t ReadWord() {
        uint8_t a, b;
        a = m_Input.get();
        b = m_Input.get();
        return ((uint16_t)b << (uint16_t)8) | (uint16_t )a;
    }

    uint32_t ReadDWord() {
        uint8_t a, b, c, d;
        a = m_Input.get();
        b = m_Input.get();
        c = m_Input.get();
        d = m_Input.get();
        return  ((uint32_t)d << (uint32_t)24 |
                (uint32_t)c << (uint32_t)16 |
                (uint32_t)b << (uint32_t)8 |
                (uint32_t)a);
    }

    void ReadBytes(uint8_t* buffer, size_t len) {
        for (int i = 0; i < (int)len; i++) {
            buffer[i] = ReadByte();
        }
    }
private:
    ifstream& m_Input;
};


#endif //FML_VC_READER_H
