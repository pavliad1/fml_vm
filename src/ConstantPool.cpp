//
// Created by Adam on 19.03.2021.
//

#include "ConstantPool.h"

#include <string>

ConstantPool::ConstantPool(int size) {
    m_Data.reserve(size);
}

ConstantPool::~ConstantPool() {

}

void ConstantPool::PushBack(Constant *object) {
    m_Data.push_back(object);
}

Constant *ConstantPool::operator[](uint16_t index) {
    return m_Data[index];
}

size_t ConstantPool::Size() const {
    return m_Data.size();
}

std::string ConstantPool::ToString() const {
    std::string out = "Constant Pool:\n";
    for (int i = 0; i < (int)m_Data.size(); i++)
        out += std::to_string(i) + ": " + m_Data[i]->ToString() + "\n";
    return out;
}
