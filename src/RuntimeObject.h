//
// Created by Adam on 19.03.2021.
//

#ifndef FML_VC_RUNTIMEOBJECT_H
#define FML_VC_RUNTIMEOBJECT_H

#include <string>
#include <cstring>
#include <stdexcept>

using namespace std;

class ROInteger;
class ROBoolean;
class RONull;
class ROString;
class ROArray;
class ROObject;

class Program;
class Heap;
class Method;

typedef uint32_t Pointer;

class RuntimeObject {
public:
    virtual bool IsInteger() const { return false; }
    virtual bool IsBoolean() const  { return false; }
    virtual bool IsString() const { return false; }
    virtual bool IsNull() const { return false; }
    virtual bool IsArray() const  { return false; }
    virtual bool IsObject() const  { return false; }

    virtual ROInteger* GetInteger() { return nullptr; }
    virtual ROBoolean* GetBoolean() { return nullptr; }
    virtual RONull* GetNull() { return nullptr; }
    virtual ROString* GetString() { return nullptr; }
    virtual ROArray* GetArray() { return nullptr; }
    virtual ROObject* GetObject() { return nullptr; }

    virtual Pointer GetField(const string& fieldName, Program& program) const {
        throw runtime_error(ToString() + " doesn't have any fields to get.");
    }
    virtual Pointer SetField(const string& fieldName, Pointer value, Program& program) {
        throw runtime_error(ToString() + " doesn't have any fields to set.");
    }

    virtual bool Equals(RuntimeObject* other) const { return false; }

    virtual void CallBuiltinMethod(const string& name, Program& program);

    virtual string ToString(Program *program = nullptr) const = 0;

    virtual std::pair<Pointer,Method*> GetReceiver(Pointer curPointer, const string& methodName, Program& program) const {
        throw runtime_error(ToString() + " has no method \"" + methodName + "\".");
    }
private:
};

class ROInteger : public RuntimeObject {
public:
    ROInteger(int value) : m_Value(value) {}
    bool IsInteger() const  override { return true; }
    int Value() const { return m_Value; }

    string ToString(Program *program = nullptr) const override {
        return to_string(m_Value);
    }

    ROInteger *GetInteger() override {
        return this;
    }

    bool Equals(RuntimeObject* other) const {
        if (!other->IsInteger())
            return false;

        return m_Value == other->GetInteger()->m_Value;
    }

    void CallBuiltinMethod(const string &name, Program &program) override;

private:


    int m_Value;
};

class ROBoolean : public RuntimeObject {
public:
    ROBoolean(bool value) : m_Value(value) {}
    bool IsBoolean() const  override  { return true; }
    bool Value() const { return m_Value; }

    string ToString(Program *program = nullptr) const override {
        return m_Value ? "true" : "false";
    }

    ROBoolean *GetBoolean() override {
        return this;
    }

    bool Equals(RuntimeObject* other) const {
        if (!other->IsBoolean())
            return false;

        return m_Value == other->GetBoolean()->m_Value;
    }

    bool And(RuntimeObject* other) const;

    bool Or(RuntimeObject* other) const;

    void CallBuiltinMethod(const string &name, Program &program) override;

private:
    bool m_Value;
};

class ROString : public RuntimeObject {
public:
    ROString(const string& value) : m_Value(value) {}
    bool IsString() const  override  { return true; }
    string& Value() { return m_Value; }
    const string& Value() const { return m_Value; }

    string ToString(Program *program = nullptr) const override {
        return m_Value;
    }

    ROString *GetString() override {
        return this;
    }
private:
    string m_Value;
};

class RONull : public RuntimeObject {
public:
    RONull() = default;
    bool IsNull() const override { return true; }

    string ToString(Program *program = nullptr) const override {
        return "null";
    }

    RONull *GetNull() override {
        return this;
    }

    bool Equals(RuntimeObject* other) const {
        if (other->IsNull())
            return true;

        return false;
    }

    void CallBuiltinMethod(const string &name, Program& program) override;

private:
};

//TODO: change this so the object fields are actually IN HEAP aka in instance of class Heap
//      IT ALSO DOESNT FREE MEMORY PROPERLY LIKE THIS
class ROArray : public RuntimeObject {
public:
    ROArray(size_t size, Pointer initialValue) : m_Size(size) {
        m_Values = new Pointer[size];
        std::fill(m_Values, m_Values + size, initialValue);
    };

    bool IsArray() const override { return true; }

    string ToString(Program *program = nullptr) const override;

    ROArray* GetArray() override { return this; }

    void CallBuiltinMethod(const string &name, Program& program) override;

    Pointer Get(size_t index) {
        if (index >= m_Size)
            throw runtime_error("Invalid index to array (get).");

        return m_Values[index];
    }

    Pointer Set(size_t index, Pointer value) {
        if (index >= m_Size)
            throw runtime_error("Invalid index to array (set).");

        m_Values[index] = value;
        return value;
    }
private:
    size_t m_Size;
    Pointer* m_Values;
};

class ROObject : public RuntimeObject {
public:
    ROObject(uint16_t index, size_t members, Pointer parent) : m_Index(index), m_Parent(parent) {
        m_Fields = new Pointer[members];
    };

    bool IsObject() const override { return true; }

    string ToString(Program *heap = nullptr) const override;

    ROObject* GetObject() override { return this; }

    uint16_t GetIndex() const {
        return m_Index;
    }

    Pointer GetParent() const {
        return m_Parent;
    }

    void SetFieldIndex(size_t index, Pointer value) {
        m_Fields[index] = value;
    }

    Pointer GetField(const string& fieldName, Program& program) const override;

    Pointer SetField(const string& fieldName, Pointer value, Program& program) override;

    void CallBuiltinMethod(const string &name, Program& program) override;

    std::pair<Pointer, Method*> GetReceiver(Pointer curPointer, const string& methodName, Program& program) const override;
private:
    std::pair<const ROObject*, size_t> GetFieldIndex(const string& fieldName, Program& program) const;

    uint16_t m_Index;   // index of the class object in constant pool
    Pointer  m_Parent;  // parent object
    Pointer* m_Fields; // allocated memory space for attributes
};

#endif //FML_VC_RUNTIMEOBJECT_H
