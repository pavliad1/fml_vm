# Compilation
The project uses Makefile -> "make" and the program should get compiled with output bin/pavliad1

# Running the program
The binary after compilation is bin/pavliad1. The program requires 2 arguments to run:

    1. argument: execute
    2. argument: file
        file is path to source file in .bc format
        

Running the program is therefor either

    ./bin/pavliad1 execute file
    or
    ./fml.sh execute file  (this file is used in test.sh to run the program)
    
